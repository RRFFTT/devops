# Docker?

## Antes vamos de containers

![containers](/images/containers.jpeg "containers")

Containers são uma unidade padronizada de software que engloba e empacota todo o código e suas dependências, possibilitando uma maneira rápida e confiável de executar aplicações em ecossistemas (máquinas) diferentes.

Imagina o seguinte, uma aplicação pode ser feita em diversas linguagens de programação, seja Java, JavaScript, Python, e ela pode também ter diversas dependências de sistema, uma biblioteca necessária para melhorar desempenho, converter imagens e por aí vai… Isso cria todo um ciclo de dependência para sua aplicação, ou seja, se alguém precisar começar a desenvolver ou até implementar em outro ambiente, todas essas dependências precisam ser pré-instaladas

![arquitetura](/images/diferença.png "arquitetura")


### O que é Docker? 

Docker é um projeto Open source que tem como objetivo automatizar a implantação de aplicativos como containers e que possam ser executados na nuvem ou até mesmo localmente.

![run](/images/run.png "run")

E outro ponto muito interessante: você pode executar os containers de imagens tanto no Linux, Windows e MacOs.

### Por que usar o Docker? 

O Docker nos ajudará a criar um ambiente executável. E sem contar que você será capaz de especificar o sistema operacional que deseja usar, a versão exata de diferentes bibliotecas, diferentes variáveis de ambiente e seus valores, entre outros pontos muito importantes. Porém, o ponto mais importante é que você será capaz de executar a sua aplicação isoladamente dentro desse ambiente.

* Ambiente padrão de desenvolvimento padrão: sim! Esse ponto é interessante! Pois usando o Docker, você poderá criar um ambiente de desenvolvimento, bem como um ambiente de produção que sejam padronizados e iguais a todos! (sem aquela historia do "na minha maquina funciona")

![work](/images/work.png "work")

### Conhecendo a arquitetura

#### Imagens

Podemos entender as imagens como formas de bolo ou template usados para criar containers.

As imagens não containers, mais dão base consistente para que haja um container.

Temos imagens oficiais ou criadas pelas comunidades.

Podemos armazenar elas localmente usando o Docker Registry,  Dockerhub, ou até Gitlab Registry.


#### Containers

Já os containers, podemos entender como bolos prontos, não podemos criar um container sem uma imagem previamente.

Os containers contém o necessário para executar uma aplicação e são baseados nas imagens.

Os mantêm o isolamento da aplicação e de recursos.

### O conceito

#### Docker Client

Já o Docker client é responsável por receber as entradas do usuário e as enviar para a engine.

Podemos ter um client e o engine na mesma máquina.

#### Docker Registry 

O Docker registry é responsável por armazenar nossas imagens, ele pode ser usado de forma local ou criar o nosso próprio servidor de imagens.

Esse servidor pode ser privado ou público, um exemplo de servidor público é o Dockerhub onde podemos encontrar diversas imagens e até subir a nossa.

#### Dockerfile

O dockerfile é um arquivo que auxiliar na criação de uma imagem , basicamente é usado instruções e elas são aplicadas em uma determinada imagem para que outra imagem seja criada baseada nas modificações.

Mais pra frente vamos aprender como criar nosso arquivo Dockerfile

#### Docker compose

O docker compose é uma ferramenta para nos auxiliar na criação de múltiplos containers Docker, com elas podemos configurar todos os parâmetros necessários para executar um container a partir de um arquivo de definição.

Nesse arquivo de execução podemos selecionar definir determinados serviços , podemos setar portas abertas, variáveis de ambiente, volumes, configurar redes e muitas possibilidade

# Bora começar 

listar as informações das instalações
> docker info 

Verificar versão 
>docker version 

listar as imagens locais
>docker imagens

listar containers
>docker ls

parar container
>docker stop 'container'

remover container
>docker rm 'container'

### Docker Exec 
Com o exec nós podemos executar qualquer comando nos nossos contêineres sem precisarmos estar no terminal deles 

* -i permite interagir com o container
* -t associa o seu terminal ao terminal do container
* -it é apenas uma forma reduzida de escrever -i -t
* --name algum-nome permite atribuir um nome ao container em execução
* -p 8080:80 mapeia a porta 80 do container para a porta 8080 do host
* -d executa o container em background


## Nossa primeira execução

Vamos rodar um "hello world"

>docker run hello-world

Vamos executar um servidor web

>docker run -it -p 8080:80 nginx

Ele irá baixar a imagem do nginx para o seu host e abrir ele na porta 8080 do seu host. Para testar assim que ele finalizar a execução do comando acima, abra no seu navegador o endereço: http://localhost:8080/

Na forma acima o terminal fica travado no container. 

Nessa forma, o terminal fica livre e o container em segundo plano 

>docker run -it -d -p 8080:80 nginx