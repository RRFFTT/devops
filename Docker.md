**Docker**

Uma plataforma aberta para criação, execução e publicação (deploy) de containers.

Um Container é a forma de empacotar sua aplicação e suas dependências (bibliotecas) de forma padronizada.

Podemos dizer que as palavras chaves para o Docker são: construir, entregar e rodar em qualquer ambiente (develop, ship and run anywhere).

**Instalação Ubuntu**:

Primeiro, atualize sua lista existente de pacotes:

`sudo apt update`

Baixe e execute o script de instalação:

`curl -fsSL [https://get.docker.com](https://get.docker.com/) -o [get-docker.sh](http://get-docker.sh/)
sh [get-docker.sh](http://get-docker.sh/)`

Verifique se ele está funcionando:

`sudo docker run hello-world`

Mais detalhes: [https://github.com/docker/docker-install](https://github.com/docker/docker-install)


# Dockerfile

Arquivo de referencia que contem todos os comandos para buildar umaimagem. tem formatação e instruções especificas.

**Instruções:**

**FROM**

**RUN**

**ADD E COPY**

**VOLUME**

**WORKDIR**
