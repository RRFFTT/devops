# Linux

O melhor e mais utilizado sistema operacional por profissionais da área.

# **Comandos Básicos**

[https://ubuntu.com/tutorials/command-line-for-beginners#3-opening-a-terminal](https://ubuntu.com/tutorials/command-line-for-beginners#3-opening-a-terminal)

**Abrir terminal:** `Ctrl-Alt-T`

**Ver o diretorio onde esta:** `pwd`

**Listar arquivos no diretorio:** `ls`

**Mudar de diretorio:** `cd diretorio`

**Voltar um diretorio:** `cd ..`

**Criar pasta:** `mkdir pasta`

**Criar arquivo:** `touch exemplo.txt`

**Mover arquivos:** `mv exemplo.txt pasta`
