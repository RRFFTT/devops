# Curso Introdução a DevOps

## O que é isso ai?
O DevOps é a combinação de filosofias culturais, práticas e ferramentas que aumentam a capacidade de uma empresa de distribuir aplicativos e serviços em alta velocidade: otimizando e aperfeiçoando produtos em um ritmo mais rápido do que o das empresas que usam processos tradicionais de desenvolvimento de software e gerenciamento de infraestrutura. 

Essa velocidade permite que as empresas atendam melhor aos seus clientes e consigam competir de modo mais eficaz no mercado.

Para funcionar, o DevOps depende de uma cultura de colaboração alinhada aos princípios open source, além de abordagens transparentes e ágeis. As equipes de desenvolvimento e operações não ficam mais separadas, estando sempre colaborando durante as etapas do processo de desenvolvimento

![Ciclo](/images/cycle.png "Ciclo")

### Cultura DevOps

A transição para o DevOps exige uma mudança de cultura e mentalidade. Em seu nível mais simples, o objetivo do DevOps é remover as barreiras entre duas equipes tradicionalmente separadas em silos: desenvolvimento e operações. 

Em algumas empresas, podem até não existir equipes de desenvolvimento e operações separadas, os engenheiros se encarregam de ambas. Com o DevOps, as duas equipes trabalham juntas para otimizar a produtividade dos desenvolvedores e a confiabilidade das operações.

 Elas se esforçam para manter a comunicação frequente, aumentar a eficiência e melhorar a qualidade dos serviços disponibilizados aos clientes.

### Processos DevOps

O desenvolvimento de aplicações modernas requer processos diferentes daqueles adotados no passado. Muitas equipes usam abordagens ágeis para o desenvolvimento de aplicações. Para essas equipes, o DevOps não é uma ideia secundária. Na verdade, a satisfação do cliente por meio da entrega antecipada e contínua de aplicações é o primeiro dos 12 princípios do Manifesto Ágil. É por isso que a abordagem de integração e implantação contínuas (CI/DI) é muito importante para as equipes de DevOps.

## Praticas

>[Aqui](PraticasDevops.md) podemos ter uma visão geral sobre as praticas de Devops e desenvolvimento ágil 


## Configurações iniciais

Para já ficar preparado para o curso e chegar afiado, temos algumas configurações/instalações que devem ser realizadas. 

Só seguir esse link 

>[Preparação](https://gitlab.com/developerexpress/devops/-/blob/master/PreparativoIniciais/Antes_da_aula.md)

